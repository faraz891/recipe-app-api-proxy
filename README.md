# Recipe App API Proxy

NGINX proxy app for aour recipe app API


# Usage


### Environment Variable

* `LISTEN_PORT` - Port to listen on (default: 8000)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the appto forward request to (default: `9000`)

